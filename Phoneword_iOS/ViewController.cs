﻿using System;
using System.Collections.Generic;
using Foundation;
using Shared.ViewModels;
using Shared.Services;
using UIKit;
using Phoneword_iOS.Services;

namespace Phoneword_iOS
{
    public partial class ViewController : UIViewController
    {
        #region Private Properties
        MainViewModel _mainViewModel;
        DialogService _dlg;
        #endregion

        public List<string> PhoneNumbers { get; set; }

        protected ViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
         }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            _mainViewModel = new MainViewModel();

            _dlg = new DialogService(this);

            #region Interactions Declarations
            TranslateButton.TouchUpInside += (object sender, EventArgs e) =>
            {
                // Convert the phone number with text to a number
                // using PhoneTranslator.cs
                //string translatedNumber = _mainViewModel.TranslateNumber(PhoneNumberText.Text);
                string translatedNumber = _mainViewModel.TranslateNumber(PhoneNumberText.Text);

                // Dismiss the keyboard if text field was tapped
                PhoneNumberText.ResignFirstResponder();

                if (translatedNumber == "")
                {
                    CallButton.SetTitle("Call ", UIControlState.Normal);
                    CallButton.Enabled = false;
                }
                else
                {
                    CallButton.SetTitle("Call " + translatedNumber,
                        UIControlState.Normal);
                    CallButton.Enabled = true;
                }
            };

            CallButton.TouchUpInside += (object sender, EventArgs e) =>
            {
                var url = new NSUrl("tel:" + _mainViewModel.Call());

                if (!UIApplication.SharedApplication.OpenUrl(url))
                {
                    var alert = UIAlertController.Create("Not supported", "Scheme 'tel:' is not supported on this device", UIAlertControllerStyle.Alert);
                    alert.AddAction(UIAlertAction.Create("Ok", UIAlertActionStyle.Default, null));
                    PresentViewController(alert, true, null);
                }
            };

            DialogButton.TouchUpInside += DialogButton_TouchUpInside;
            #endregion
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);

            var callHistoryController = segue.DestinationViewController as CallHistoryController;

            if (callHistoryController != null)
            {
                callHistoryController.MainVM = _mainViewModel;
            }
        }


        #region Button Events Handlers
        private void DialogButton_TouchUpInside(object sender, EventArgs e)
        {
            _mainViewModel.AskQuestion(_dlg, (t) => { AnswerLabel.Text = t; });
        }

        #endregion
    }
}
