using Foundation;
using Shared.ViewModels;
using System;
using System.Collections.Generic;
using UIKit;

namespace Phoneword_iOS
{
    public partial class CallHistoryController : UITableViewController
    {
        public MainViewModel MainVM;

        static NSString callHistoryCellId = new NSString("CallHistoryCell");

        public CallHistoryController (IntPtr handle) : base (handle)
        {
            TableView.RegisterClassForCellReuse(typeof(UITableViewCell), callHistoryCellId);
            TableView.Source = new CallHistoryDataSource(this);
        }

        class CallHistoryDataSource : UITableViewSource
        {
            CallHistoryController controller;

            public CallHistoryDataSource (CallHistoryController controller)
            {
                this.controller = controller;
            }

			public override nint RowsInSection(UITableView tableview, nint section)
			{
                return controller.MainVM.CallsHistory.Count;
			}

			public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
			{
                var Cell = tableView.DequeueReusableCell (CallHistoryController.callHistoryCellId);

                int row = indexPath.Row;
                Cell.TextLabel.Text = controller.MainVM.CallsHistory[row];
                return Cell;
			}
		}
    }
}