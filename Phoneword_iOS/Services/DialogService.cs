﻿using System;
using Shared.Services;
using UIKit;

namespace Phoneword_iOS.Services
{
    public class DialogService : IDialogService
    {
        #region Private Properties
        private UIViewController controller;
        #endregion

        #region Public Properties

        #endregion

        #region Ctor
        public DialogService(UIViewController controller)
        {
            this.controller = controller;
        }
        #endregion

        #region Interface Implementation
        public void AskQuestion(Action<string> action)
        {
            var alert = UIAlertController.Create("Question", "Donnez moi une réponse", UIAlertControllerStyle.Alert);

            alert.AddTextField((field) => {
                field.Placeholder = "Quelle est votre réponse ?";
            });
            alert.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, null));
            alert.AddAction(UIAlertAction.Create("Answer", UIAlertActionStyle.Default, (a) =>
            {
                action(alert.TextFields[0].Text);
            }));

            controller.PresentViewController(alert, animated: true, completionHandler: null);
        }
        #endregion
    }
}
