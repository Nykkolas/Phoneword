﻿using System;
using System.Text;

namespace Shared.Services
{
    public interface IDialogService
    {
        void AskQuestion(Action<string> action);
    }
}
