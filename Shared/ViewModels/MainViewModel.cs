﻿using System;
using System.Collections.Generic;
using System.Text;
using Shared.Services;

namespace Shared.ViewModels
{
    public class MainViewModel
    {
        #region Private Properties
        private string _lastTranslated;
        private List<string> _callsHistory;
        private string _answer;
        #endregion

        #region Computed Properties
        public string LastTranslated
        {
            get { return _lastTranslated; }
        }

        public List<string> CallsHistory
        {
            get { return _callsHistory; }
        }

        public string Answer
        {
            get { return _answer; }
        }
        #endregion

        #region Ctor
        public MainViewModel()
        {
            _callsHistory = new List<string>();
        }
        #endregion

        #region Private Methods
        private char TranslateChar(char c)
        {
            if ("ABC".IndexOf(c) >= 0)
                return '1';
            if ("DEF".IndexOf(c) >= 0)
                return '2';
            if ("GHI".IndexOf(c) >= 0)
                return '3';
            if ("JKL".IndexOf(c) >= 0)
                return '4';
            if ("MNO".IndexOf(c) >= 0)
                return '5';
            if ("PQR".IndexOf(c) >= 0)
                return '6';
            if ("STU".IndexOf(c) >= 0)
                return '7';
            if ("VWX".IndexOf(c) >= 0)
                return '8';
            if ("YZ".IndexOf(c) >= 0)
                return '9';
            return c;
        }
        #endregion

        #region Public Methods
        public string TranslateNumber(string number)
        {
            if (String.IsNullOrEmpty(number))
                return number;

            var translatedNumber = new StringBuilder();
            foreach (char c in number)
            {
                translatedNumber.Append(TranslateChar(c));
            }

            _lastTranslated = translatedNumber.ToString();

            return LastTranslated;
        }

        public string Call()
        {
            _callsHistory.Add(LastTranslated);
            return LastTranslated;
        }

        public void AskQuestion (IDialogService dlg, Action<string> action)
        {
            action += (t) => { _answer = t; };
            dlg.AskQuestion(action);
        }
            
        #endregion
    }
}
