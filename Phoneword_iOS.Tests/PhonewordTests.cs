﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Xamarin.UITest;
using Xamarin.UITest.iOS;
using Xamarin.UITest.Queries;

namespace Phoneword_iOS.Tests
{
    [TestFixture]
    public class PhonewordTests
    {
        iOSApp app;

		private static class ScreenObjectsEnum
		{
			public const string CallHistoryButton = "CallHistoryButton",
			DialogButton = "DialogButton",
			PhoneNumberText = "PhoneNumberText",
			TranslateButton = "TranslateButton",
			CallButton = "CallButton";
		}

		[SetUp]
		public void BeforeEachTest()
        {
            // TODO: If the iOS app being tested is included in the solution then open
            // the Unit Tests window, right click Test Apps, select Add App Project
            // and select the app projects that should be tested.
            //
            // The iOS project should have the Xamarin.TestCloud.Agent NuGet package
            // installed. To start the Test Cloud Agent the following code should be
            // added to the FinishedLaunching method of the AppDelegate:
            //
            //    #if ENABLE_TEST_CLOUD
            //    Xamarin.Calabash.Start();
            //    #endif

			// SetUp : Xamarin.UITest.XDB.Exceptions.ExternalProcessException : xcodebuild output not as expected
			// https://stackoverflow.com/questions/43990356/xamarin-xcodebuild-output-not-as-expected
            app = ConfigureApp
                .iOS
                // TODO: Update this path to point to your iOS app and uncomment the
                // code if the app is not included in the solution.
				//.AppBundle("../../../Phoneword_iOS/bin/iPhoneSimulator/Debug/Phoneword_iOS.app")
				//.AppBundle("/Users/nicolasfournier/Projects/Phoneword/Phoneword_iOS/bin/iPhoneSimulator/Debug/Phoneword_iOS.app")
                .StartApp();
        }
        
        [Test]
        public void AppLaunches()
        {
			app.Query(c => c.Id(ScreenObjectsEnum.PhoneNumberText).Invoke("setText", "123-TOTO"));
		             
			app.Tap(c => c.Id(ScreenObjectsEnum.TranslateButton));

			var results = app.Query(c => c.Id(ScreenObjectsEnum.CallButton).Invoke("currentTitle"));

			Assert.AreEqual(1, results.Length);
			Assert.AreEqual("Call 123-7575", results[0]);
        }
    }
}
