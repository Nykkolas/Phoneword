﻿
using System;
using System.Text;
using Moq;
using NUnit.Framework;
using Shared.Services;
using Shared.ViewModels;

namespace Shared.Tests
{
    [TestFixture]
    public class MainViewModelTests
    {
        #region Properties
        MainViewModel Vm;
        #endregion

        #region SetUp
        [SetUp]
        public void SetUp()
        {
            Vm = new MainViewModel();
        }
        #endregion

        #region Ctor Tests
        [Test]
        public void MainViewModel_WhenCalled_ThenConstruct()
        {
            //Arrange

            //Act

            //Asert
            Assert.IsNotNull(Vm);
        }
        #endregion

        #region TranslateNumber Tests
        [Test]
        public void TranslateNumber_WhenEmptyString_ThenReturnEmptyString()
        {
            //Arrange

            //Act
            string result = Vm.TranslateNumber("");

            //Assert
            Assert.True(String.IsNullOrEmpty(result));
        }

        [Test]
        public void TranslateNumber_WhenOneNumber_ThenReturnThisNumber()
        {
            //Arrange

            //Act
            string result = Vm.TranslateNumber("1");

            //Assert
            Assert.AreEqual("1", result);
        }

        [Test]
        public void TranslteNumber_WhenA_ThenOne()
        {
            //Arrange

            //Act
            string result = Vm.TranslateNumber("A");

            //Asert
            Assert.AreEqual("1", result);
        }

        //[Test]
        //public void TranslateNumber_WhenABC_ThenReturnOne()
        //{
        //    //Arrange

        //    //Act
        //    string resultA = Vm.TranslateNumber("A");
        //    string resultB = Vm.TranslateNumber("B");
        //    string resultC = Vm.TranslateNumber("C");

        //    //Assert
        //    Assert.AreSame("1", resultA);
        //    Assert.AreSame("1", resultB);
        //    Assert.AreSame("1", resultC);
        //}

        [Test]
        public void TranslateNumber_When1A_ThenReturn11()
        {
            //Arrange

            //Act
            string result = Vm.TranslateNumber("1A");

            //Assert
            Assert.AreEqual("11", result);
        }

        [Test]
        public void TranslateNumber_WhenADGJMPSVY_Then123456789()
        {
            //Arrange

            //Act
            string result = Vm.TranslateNumber("ADGJMPSVY");

            //Assert
            Assert.AreEqual("123456789", result);
        }

        [Test]
        public void TranslateNumber_WhenTranslate_ThenRemember()
        {
            //Arrange

            //Act
            Vm.TranslateNumber("E-4");

            //Assert
            Assert.AreEqual("2-4", Vm.LastTranslated);
        }
        #endregion

        #region Call Tests
        [Test]
        public void Call_WhenCalled_ThenReturnLastTranslated()
        {
            //Arrange
            Vm.TranslateNumber("4-Y-");

            //Act
            string result = Vm.Call();

            //Assert
            Assert.AreEqual("4-9-", result);
        }

        [Test]
        public void Call_WhenCall_ThenAddToHistory()
        {
            //Arrange
            Vm.TranslateNumber("123");

            //Act
            string result = Vm.Call();

            //Assert
            Assert.AreEqual("123", Vm.CallsHistory[0]);
        }
        #endregion

        #region AskQuestion
        [Test]
        public void AskQuestion_WhenCancel_ThenDoNothing()
        {
            //Arrange
            var dlgMock = new Mock<IDialogService>();
            IDialogService dlg = dlgMock.Object;
            string text = "";
            dlgMock.Setup(d => d.AskQuestion(It.IsAny<Action<string>>()))
                   .Verifiable();

            //Act
            Vm.AskQuestion(dlg, (t) => { text = t; });

            //Assert
            dlgMock.Verify(d => d.AskQuestion(It.IsAny<Action<string>>()));
            Assert.IsEmpty(text);
        }

        [Test]
        public void AskQuestion_WhenAnswer_ThenValueSaved()
        {
            var dlgMock = new Mock<IDialogService>();
            IDialogService dlg = dlgMock.Object;
            string expected = "Voici ma réponse";
            string text = "";
            Action<string> callArgs = null;
            dlgMock.Setup(d => d.AskQuestion(It.IsAny<Action<string>>()))
                   .Callback<Action<string>>((a) => { callArgs += a; });

            //Act
            Vm.AskQuestion(dlg, t => { text = t; });
            callArgs(expected);

            //Assert
            dlgMock.Verify(d => d.AskQuestion(It.IsAny<Action<string>>()));
            Assert.AreEqual(expected, text);
            Assert.AreEqual(expected, Vm.Answer);
        }
        #endregion
    }
}
